const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser')
const logger = require('morgan');
const proxy = require('http-proxy-middleware');

const { NODE_ENV, API_HOST = 'localhost', API_PORT = 3000 } = process.env;

const indexRouter = require('./routes/index');

const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(express.static(path.join(__dirname, 'public')));

if (NODE_ENV === 'development') {
  app.use('/api/v1', proxy({ target: `http://${API_HOST}:${API_PORT}` }));
}
app.use('/', indexRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  console.error('ERROR', err);
  const { theme = 'light' } = req.cookies;
  
  res.status(err.response ? err.response.status : err.status || 500);
  res.render('pages/error', {
    err: req.app.get('env') === 'development' ? {
      message: err.response ? err.response.statusText : err.message,
      status: err.response ? err.response.status : err.status || 500,
      stack: err.stack,
    } : {
      message: err.response ? err.response.statusText : err.message,
      status: err.response ? err.response.status : err.status || 500,
    },
    theme,
  });
});

module.exports = app;
