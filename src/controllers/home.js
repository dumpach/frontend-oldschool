const axios = require('axios');

const config = require('../config');

module.exports = {
  get: async (req, res, next) => {
    const { theme = 'light' } = req.cookies;
    
    try {
      const { data } = await axios(`${config.app.api.endpoint}/boards`);
      res.render('pages/home', { theme, sections: data.data });
    } catch (err) {
      next(err);
    }
  }
}