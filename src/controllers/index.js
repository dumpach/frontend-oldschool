const boardController = require('./board');
const homepageController = require('./home');
const threadController = require('./thread');

module.exports = {
  boardController,
  homepageController,
  threadController,
};
