const axios = require('axios');

const config = require('../config');

module.exports = {
  get: async (req, res, next) => {
    const { boardId, threadId } = req.params;
    const { theme = 'light' } = req.cookies;

    try {
      const { data } = await axios(`${config.app.api.endpoint}/boards/${boardId}/threads/${threadId}`);
      res.render('pages/thread', { theme, thread: data.data });
    } catch (err) {
      next(err);
    }
  }
}