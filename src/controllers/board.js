const axios = require('axios');
const request = require('request-promise');
// const Busboy = require('busboy');

const config = require('../config');

module.exports = {
  get: async (req, res, next) => {
    const { boardId } = req.params;
    const { theme = 'light' } = req.cookies;

    try {
      const { data } = await axios(`${config.app.api.endpoint}/boards/${boardId}/threads`);
      res.render('pages/board', { theme, boardId, threads: data.data });
    } catch (err) {
      next(err);
    }
  },
}