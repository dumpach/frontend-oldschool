const express = require('express');
const router = express.Router();

const { 
  boardController,
  homepageController,
  threadController
} = require('../controllers');

router.get('/', homepageController.get);
router.get('/boards/:boardId', boardController.get);
router.get('/boards/:boardId/:pageId', boardController.get);
router.get('/boards/:boardId/threads/:threadId', threadController.get);

module.exports = router;
