const { NODE_ENV, API_HOST = 'localhost', API_PORT = 3000 } = process.env;

const development = {
  api: {
    endpoint: `http://${API_HOST}:${API_PORT}/api/v1`,
  },
};

const config = {
  development,
};

const exportedConfig = config[NODE_ENV] || config['development'];

module.exports = exportedConfig;