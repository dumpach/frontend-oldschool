document.addEventListener("DOMContentLoaded", () => {
  const theme = localStorage.getItem('theme') || 'light';

  const link = document.createElement('link')
  link.rel = 'stylesheet';
  link.href = `/stylesheets/themes/${theme}.css`;
  document.head.appendChild(link);
});
