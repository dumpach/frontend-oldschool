import { setCookie } from '../utils/cookies.js';

const changeTheme = (event) => {
  localStorage.setItem('theme', event.target.value);

  setCookie('theme', event.target.value);

  const link = document.createElement('link')
  link.rel = 'stylesheet';
  link.href = `/stylesheets/themes/${event.target.value}.css`;
  document.head.appendChild(link);
}

const changeThemeSelect = document.querySelector('#change-theme-select');

changeThemeSelect.addEventListener('change', changeTheme);

