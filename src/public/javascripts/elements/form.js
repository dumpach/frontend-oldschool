const sendForm = (event) => {
  event.preventDefault();
  const formData = new FormData(form);
  const { files } = document.querySelector('#thread-form-files');
  if (files.length === 0) {
    formData.delete('files');
  }
  fetch(`/api/v1${window.location.pathname}/threads`, {
    method: 'POSt',
    body: formData,
  })
  .then((res) => res.json())
  .then((data) => {
		if (data.error) {
      console.log('TCL: sendForm -> data.error', data.error);
      return;
    }
    
    window.location.href = `/boards/${data.data.board_id}/threads/${data.data.id}`;
  })
  .catch((err) => {
		console.log('TCL: sendForm -> err', err);
  })
}

const form = document.querySelector('#thread-form');

form.addEventListener('submit', sendForm);
