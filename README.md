# Frontend Oldschool

This is pure ssr frontend tryout with express and pug.

## Prepare

Clone some repos:
  - https://gitlab.com/dumpach/docker - main docker-compose files repo
  - https://gitlab.com/dumpach/backend - backend, lol
  - https://gitlab.com/dumpach/nginx - if you want try prodlike environment
  - this repo

## Run

Uncomment frontend-oldschool service in `development.docker-compose.yml` or in `production.development.docker-compose.yml` for prodlike environment.

Run from `docker` folder: 
```bash
docker-compose -f development.docker-compose.yml
```